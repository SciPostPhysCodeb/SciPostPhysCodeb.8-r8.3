# Codebase release 8.3 for PYTHIA

by Christian Bierlich, Smita Chakraborty, Nishita Desai, Leif Gellersen, Ilkka Helenius, Philip Ilten, Leif Lönnblad, Stephen Mrenna, Stefan Prestel, Christian T. Preuss, Torbjörn Sjöstrand, Peter Skands, Marius Utheim, Rob Verheyen

SciPost Phys. Codebases 8-r8.3 (2022) - published 2022-11-10

[DOI:10.21468/SciPostPhysCodeb.8-r8.3](https://doi.org/10.21468/SciPostPhysCodeb.8-r8.3)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.8-r8.3) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Christian Bierlich, Smita Chakraborty, Nishita Desai, Leif Gellersen, Ilkka Helenius, Philip Ilten, Leif Lönnblad, Stephen Mrenna, Stefan Prestel, Christian T. Preuss, Torbjörn Sjöstrand, Peter Skands, Marius Utheim, Rob Verheyen

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.8-r8.3](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.8-r8.3)
* Live (external) repository at [https://gitlab.com/Pythia8/releases/-/tree/pythia8308](https://gitlab.com/Pythia8/releases/-/tree/pythia8308)